import json

from dotenv import load_dotenv

load_dotenv()


from mongoengine import connect
connect('jumiabot-db', host="mongodb://127.0.0.1:27017/jumiabot-db")

from app.models import ProductCategory, ProductBrand


product_brands = ProductBrand.objects[:180]
product_categories = ProductCategory.objects[:30]

# entry = {
#     'value': 'product',
#     'synonyms': []
# }

brand_entity = {
    'name': 'products-entity',
    'automatedExpansion': True,
    'entries': []
}

category_enity = {
    'name': 'products-categories',
    'automatedExpansion': True,
    'entries': []
}

for brand in product_brands:
    if brand is not None and brand.title is not None:
        brand_entity['entries'].append({
            'value': brand.title,
            'synonyms':  [brand.title.strip().lower(), brand.title.strip().upper()]
        })


with open('product-brands-entity.json', 'w') as outfile:
    json.dump(brand_entity, outfile, ensure_ascii=False)


for product_category in product_categories:
    if product_category is not None and product_category.title is not None and product_category.title != 'personal protective equipment (ppe)':
        category_enity['entries'].append({
            'value': product_category.title,
            'synonyms':  [product_category.title.strip().lower(), product_category.title.strip().upper()]
        })



with open('product-categories-entity.json', 'w') as outfile:
    json.dump(category_enity, outfile, ensure_ascii=False)

